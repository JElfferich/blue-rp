local PRINT_TRIM = '@addons/blue-rp/lua/'

if defaultPrint == nil then
  defaultPrint = print
end

function print(...)
  local debugInfo = debug.getinfo(2, 'lS')
  defaultPrint(string.format('[BlueRP] %s:%s: ', string.TrimLeft(debugInfo.source, PRINT_TRIM), debugInfo.currentline), ...)
end
