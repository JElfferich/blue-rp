include 'shared/helpers/path.lua'
include 'shared/helpers/print.lua'

function copy(obj, seen)
  if type(obj) ~= 'table' then return obj end
  if seen and seen[obj] then return seen[obj] end
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do res[copy(k, s)] = copy(v, s) end
  return res
end

function merge(a, b)
    if type(a) == 'table' and type(b) == 'table' then
        for k,v in pairs(b) do if type(v)=='table' and type(a[k] or false)=='table' then merge(a[k],v) else a[k]=v end end
    end
    return a
end

function debounce( func, delay )
  delay = delay or 0.1
  local last = 0

  return function ( ... )
      local now = SysTime()
      local dt = now - last
      if dt < delay then return end;
      last = now
      return func( ... )
  end
end

function wrapHook( name, func, ... )
  local hookArgs = { ... }

  return function ( ... )
    local result = func( ... )
    hook.Run( name, unpack( hookArgs ) )
    return result
  end
end
