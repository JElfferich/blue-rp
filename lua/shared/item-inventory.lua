ItemInventory = { itemsMax = 20 }

function ItemInventory:new(opts)
  opts = merge( { items = {} }, opts or {} )
  setmetatable(opts, self)
  self.__index = self
  return opts
end

function ItemInventory:addItem(gameItem)
  if self:isFull() then
    return false
  end

  table.insert(self.items, gameItem)
  
  return #self.items
end

function ItemInventory:removeItem(idx)
  return table.remove(self.items, idx)
end

function ItemInventory:getItems()
  return self.items
end

function ItemInventory:isFull()
  return self.itemsMax ~= nil and #self.items >= self.itemsMax
end
