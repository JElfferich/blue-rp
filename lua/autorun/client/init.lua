print('BlueRP client autorun start')

include 'shared/shared.lua'
include 'client/constants.lua'
include 'client/store.lua'
include 'client/net.lua'
include 'client/fonts.lua'
include 'client/ui.lua'

print('BlueRP client autorun end')
