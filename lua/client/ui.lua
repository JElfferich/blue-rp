local function load()
  include 'client/ui/item-inventory.lua'
end

hook.Add('OnGamemodeLoaded', 'client_init_ui', function()
  load()
  gamemodeLoaded = true
end)

if gamemodeLoaded then
  load()
end
