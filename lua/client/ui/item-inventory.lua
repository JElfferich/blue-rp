local itemInventory = {}

function itemInventory:init()
  -- create frame
  local frame = vgui.Create( 'DFrame', nil ) 
  frame:SetSize( 300, 300 )
  frame:Center()
  frame:SetTitle( 'BlueRP Inventory' ) 
  frame:SetVisible( true ) 
  frame:SetDraggable( true ) 
  -- frame:SetSizable( true )
  frame:ShowCloseButton( false ) 
  frame:MakePopup()
  frame:Hide()
  self.frame = frame

  self.statPanel = vgui.Create( 'Panel', frame )
  self.statPanel:DockMargin( 0, 0, 12, 0 )
  self.statPanel:Dock( LEFT )
  
  self.itemsPanel = vgui.Create( 'DPanel', frame )
  self.itemsPanel:Dock( LEFT )

  self:update()
end

function itemInventory:update()
  self.itemsPanel:Clear()
  self.itemsPanel:InvalidateChildren()

  local inventory = store.itemInventory
  local items = inventory:getItems()
  local itemsMax = inventory.itemsMax

  -- create grid
  local grid = vgui.Create( 'DGrid', self.itemsPanel )
  grid:SetPos( 0, 24 )
  grid:SetCols( 6 )
  grid:SetColWide( 64 )
  grid:SetRowHeight( 64 )
  grid:Dock( TOP )

  -- create spawn icons
  local itemsByModel = {}
  
  for idx, item in ipairs( items ) do
    if itemsByModel[item.model] == nil then itemsByModel[item.model] = {} end
    table.insert( itemsByModel[item.model], { idx = idx, item = item } )
  end
  
  for model, modelItems in pairs( itemsByModel ) do
    local topIdx = modelItems[ #modelItems ].idx
    local itemsCount = #modelItems
    
    local spawnIcon = vgui.Create( 'SpawnIcon', self.itemsPanel )
    spawnIcon:SetModel( model )
    spawnIcon:SetSize( 64, 64 )
    spawnIcon.DoClick = function ()
      netActions.itemInventory.dropItem( topIdx )
    end
    
    local spawnItemCountBackground = vgui.Create( 'DPanel', spawnIcon )
    spawnItemCountBackground:SetBackgroundColor( Color(255, 255, 255, 134) )
    spawnItemCountBackground:SetMouseInputEnabled( false )

    local spawnItemCount = vgui.Create( 'DLabel', spawnItemCountBackground )
    spawnItemCount:SetText( itemsCount )
    spawnItemCount:SetColor( Color( 0, 0, 0, 225) )
    spawnItemCount:SetFont( 'SpawnIconCount' ) 
    spawnItemCount:SizeToContents()
    spawnItemCountBackground:SizeToChildren( true, true )
    spawnItemCountBackground:InvalidateLayout( true )

    spawnItemCountBackground:AlignRight( 4 )
    spawnItemCountBackground:AlignBottom( 4 )
    
    grid:AddItem( spawnIcon )
  end

  -- update stat panel
  self.statPanel:Clear()
  self.statPanel:InvalidateChildren()

  local labelMaxItems = vgui.Create( 'DLabel', self.statPanel )
  labelMaxItems:SetText( string.format( 'Items: %d/%d', #items, itemsMax ) )
  labelMaxItems:SizeToContents()
  labelMaxItems:Dock( TOP )

  self.itemsPanelGrid = grid

  -- autoresize
  for _, p in ipairs( { self.itemsPanelGrid, self.itemsPanel, self.statPanel, self.frame } ) do
    p:SizeToChildren( true, true )
    p:InvalidateLayout( true )
  end
end

itemInventory:init()

hook.Add('OnContextMenuOpen', 'item_inventory_show', function ()
  itemInventory.frame:Show()
end)

hook.Add('OnContextMenuClose', 'item_inventory_show', function ()
  itemInventory.frame:Hide()
end)

hook.Add('store.itemInventory.addItem', 'item_inventory_update_items', function ()
  itemInventory:update()
end)

hook.Add('store.itemInventory.removeItem', 'item_inventory_update_items', function ()
  itemInventory:update()
end)
