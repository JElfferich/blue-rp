netActions.itemInventory = {}

net.Receive( 'item_inventory.update', function ()
  local itemInventory = net.ReadTable()
  store.itemInventory = itemInventory
end )

net.Receive( 'item_inventory.add_item', function ()
  local item = Item:new {
    model = net.ReadString()
  }

  store.itemInventory:addItem( item );
end )

function netActions.itemInventory.dropItem( idx )
  store.itemInventory:removeItem( idx )

  net.Start( 'item_inventory.drop_item' )
    net.WriteUInt( idx, 16 )
  net.SendToServer()
end
