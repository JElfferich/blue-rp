local itemInventory = ItemInventory:new()

-- wrap methods for hook support
itemInventory.addItem = wrapHook( 'store.itemInventory.addItem', itemInventory.addItem, itemInventory )
itemInventory.removeItem = wrapHook( 'store.itemInventory.removeItem', itemInventory.removeItem, itemInventory )

store.itemInventory = itemInventory
