netActions = {}

include 'server/net/item-inventory.lua'

function netActions.lootItem(ply, ent)
    local itemInventory = store.players[ ply ].store.itemInventory

    if itemInventory:isFull() then return end

    local item = Item:new {
        model = ent:GetModel()
    }

    netActions.itemInventory.addItem(ply, item)
    ent:Remove()
end
