function addCSLuaDir(dir)  
  local files, directories = file.Find(dir .. '/*', 'LUA')
  
  for i, directoryName in ipairs(directories) do
    addCSLuaDir(dir .. '/' .. directoryName)
  end
  
  for i, fileName in ipairs(files) do
    AddCSLuaFile(dir .. '/' .. fileName)
  end
end

for i, dir in ipairs( { 'client', 'shared' } ) do
  addCSLuaDir(dir)
end
