netActions.itemInventory = {}

util.AddNetworkString 'item_inventory.add_item'
function netActions.itemInventory.addItem( ply, item )
    store.players[ply].store.itemInventory:addItem( item );

    net.Start( 'item_inventory.add_item' )
        net.WriteString( item.model )
    net.Send( ply )
end

util.AddNetworkString 'item_inventory.drop_item'
net.Receive( 'item_inventory.drop_item', function ( len, ply )
    local maxSpawnDistance = 60
    local playerInventory = store.players[ ply ].store.itemInventory
    local idx = net.ReadUInt( 16 )

    -- validate
    if playerInventory:getItems()[ idx ] == nil then print( string.format( 'idx %d does not exist in %s inventory.', idx, ply:GetName() ) ) return end

    -- remove item from inventory and store it locally
    local item = playerInventory:removeItem( idx )

    -- calculate spawn position
    local eyeTrace = ply:GetEyeTrace()
    local eyeDir = ( eyeTrace.HitPos - eyeTrace.StartPos ):GetNormalized()
    local distance = eyeTrace.StartPos:Distance(eyeTrace.HitPos)
    local spawnPos = eyeTrace.StartPos + eyeDir * math.min( distance, maxSpawnDistance )

    local ent = ents.Create( 'prop_physics' )
    ent:SetModel( item.model )
    ent:SetPos( spawnPos )
    ent:Spawn()
end )
