if store == nil then 
  store = {
    players = {}
  }
end

hook.Add('PlayerInitialSpawn', 'store_add_player', function (ply)
  store.players[ply] = {
    store = {
      itemInventory = ItemInventory:new()
    }
  }
end)

hook.Add('PlayerDisconnected', 'store_remove_player', function (ply)
  table.remove(store.players, ply)
end)
